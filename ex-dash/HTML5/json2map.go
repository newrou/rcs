package main

import (
    "encoding/json"
    "fmt"
    "strings"
    "log"
)

func isMap(x interface{}) bool {
    t := fmt.Sprintf("%T", x)
    return strings.HasPrefix(t, "map[")
}

/*
func print_map(m map[string]interface{}) {
// func print_map(m interface{}) {
// func print_map(m map[string]string) {
    for key, value := range m {
//	fmt.Printf("%T\t\t", value)
//	if fmt.Sprintf("%T", value)=="string" {
	if isMap(value) {
	    fmt.Println(key, ": *** ")
	    mapAny := make(map[string]interface{})
	    for key, val := range value { mapAny[key] = val }
	    print_map(mapAny)
	} else {
	    fmt.Println(key, ":", value)
	}
    }
}
*/

func print_map(x interface{}, level int) {
    x_type := fmt.Sprintf("%T", x)
    ind := strings.Repeat("  ", level)
//    ind2 := strings.Repeat("  ", level+1)
    switch x_type {
	case "map[string]interface {}":
	    fmt.Println(ind, "map:")
	    for key, value := range x.(map[string]interface{}) {
		fmt.Print(ind, "   key: ", key, " ")
		print_map(value, level+1)
	    }
	case "[]interface {}":
	    fmt.Println(" array:")
	    for _, value := range x.([]interface{}) {
//		fmt.Println(ind2, "k: ", k)
		print_map(value, level+1)
	    }
	case "string":
	    fmt.Println("string: ", x)
	default:
	    fmt.Println(ind, "type: ", x_type, " ", x)
    }
}

func print_map2(v map[string]interface{}) {
    fmt.Printf("type: %T\n", v)
    for _, record := range v {
	log.Printf(" [===>] Record: %s\n", record)
	if rec, ok := record.(map[string]interface{}); ok {
	    for key, val := range rec { 
		log.Printf(" [========>] %s = %s\n", key, val) 
	    }
	} else {
//	    fmt.Printf("record not a map[string]interface{}: %v\n", record)
	    fmt.Printf("type: %T\n", v)
	    fmt.Println(record)
	}
    }
}

// main function
func main() {

    // defining a map
    var result map[string]interface{}

    // string json
    jsonString := `{
	"results": [
	  {
	    "nameChunk1": [
	      {
		"name1": "Anna",
		"name2": "Bob"
	      }
	    ],
	    "nameChunk2": [
	      {
		"name1": "Clay",
		"name2": "Dean"
	      }
	    ]
	  }
	],
	"author": "Go Linux Cloud"
      }`

    err := json.Unmarshal([]byte(jsonString), &result)
    if err != nil { fmt.Println(err) }

    fmt.Println(jsonString)
//    fmt.Println(result)

//    for key, value := range result {
//	fmt.Printf("%T\t\t", value)
//	if fmt.Sprintf("%T", value)=="string" { fmt.Println(key, ":", value) } else { fmt.Println(key, ": ###", value) }
//    }

    print_map(result, 0)
}

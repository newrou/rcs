package main

import (
    "log"
    "io/ioutil"
    "os"
    "fmt"
    "strings"
    "encoding/json"
)

func isMap(x interface{}) bool {
    t := fmt.Sprintf("%T", x)
    return strings.HasPrefix(t, "map[")
}

func print_map(x interface{}, level int) {
    x_type := fmt.Sprintf("%T", x)
    ind := strings.Repeat("  ", level)
    switch x_type {
	case "map[string]interface {}":
	    fmt.Println(ind, "map:")
	    for key, value := range x.(map[string]interface{}) {
		fmt.Print(ind, "   key: ", key, " ")
		print_map(value, level+1)
	    }
	case "[]interface {}":
	    fmt.Println(" array:")
	    for _, value := range x.([]interface{}) {
		fmt.Print(ind)
		print_map(value, level+2)
	    }
	case "string":
	    fmt.Println("string: ", x)
	default:
	    fmt.Println(ind, "type: ", x_type, " ", x)
    }
}

func print_form_old(frm map[string]interface{}) {
    Title := frm["title"]
    Description := frm["description"]
    Type := frm["type"]
    Required := frm["required"]
    Properties := frm["properties"]
    fmt.Println("<br>Title: ", Title)
    fmt.Println("<br>Description: ", Description)
    fmt.Println("<br>Type: ", Type)
    fmt.Println("<br>Required: ", Required)
    fmt.Println("<br>Properties: ", Properties)
    for name, opts := range Properties.(map[string]interface{}) {
	opt := opts.(map[string]interface{})
	fmt.Print("    <p><label for=", name, ">", opt["title"], ": </label> <input ")
	for k, v := range opts.(map[string]interface{}) {
	    fmt.Print(k, "=\"",v,"\" ")
	}
	fmt.Print("></p>\n")
    }
}

func print_form(frm map[string]interface{}) {
    Title := frm["title"]
    Description := frm["description"]
    Type := frm["type"]
    Required := frm["required"]
    Properties := frm["properties"].(map[string]interface{})
    Order := frm["order"]
    fmt.Println("<br>Title: ", Title)
    fmt.Println("<br>Description: ", Description)
    fmt.Println("<br>Type: ", Type)
    fmt.Println("<br>Required: ", Required)
    fmt.Println("<br>Properties: ", Properties)
    fmt.Println("<br>Order: ", Order)
    for _, name := range Order.([]interface{}) {
	opts := Properties[name.(string)].(map[string]interface{})
//	opt := opts.(map[string]interface{})
//	fmt.Println(i, name, opts)
	fmt.Print("    <p><label for=", name.(string), ">", opts["title"], ": </label> <input ")
	for k, v := range opts {
	    fmt.Print(k, "=\"",v,"\" ")
	}
	fmt.Print("></p>\n")
    }
}

func load_file(fname string) string {
    content, err := ioutil.ReadFile(fname)
    if err != nil { 
	log.Fatal("Error when opening file: ", err)
	return "Error when opening file!"
    }
    return string(content)
}

func main() {
    var result map[string]interface{}

    jsonString := load_file(os.Args[1])

    err := json.Unmarshal([]byte(jsonString), &result)
    if err != nil { fmt.Println(err) }

//    fmt.Println(jsonString)
//    print_map(result, 0)
    fmt.Println(load_file("html5.head"))
    print_form(result)
    fmt.Println(load_file("html5.end"))
}

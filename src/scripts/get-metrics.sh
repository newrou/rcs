#!/bin/bash

LC_NUMERIC="en_US.UTF-8"

unixtime=`date +%s`
memtotal=`grep "MemTotal:" < /proc/meminfo | awk '{ print $2 }'`
memfree=`grep "MemFree:" < /proc/meminfo | awk '{ print $2 }'`
memused=$(($memtotal - $memfree))

ncpu=`grep 'processor' < /proc/cpuinfo | wc -l`
cpuload=`cut -f 1 -d ' ' < /proc/loadavg`
#cpuload="0.0050"
#allcpuload=`bc -l <<< "scale=4; $cpuload/$ncpu.0" | tr "." ","`
allcpuload=`bc -l <<< "scale=4; $cpuload/$ncpu.0"`
#echo $allcpuload
#allcpuload=`printf %.4f "0$allcpuload" | tr "," "."`
allcpuload=`printf %.4f "0$allcpuload"`

if [ ! -f metrics.csv ]; then
    echo "unixtime; memtotal; memfree; memused; cpuload; allcpuload" | tee metrics.csv
fi
echo "$unixtime; $memtotal; $memfree; $memused; $cpuload; $allcpuload" | tee -a metrics.csv

if [ ! -f rcs-fs.csv ]; then
    echo "unixtime; fsname; used; available" | tee rcs-fs.csv
fi
zfs list -p | tail -n +2 | awk -v ut=$unixtime '{ print ut"; "$1"; "$2"; "$3 }' | tee -a rcs-fs.csv
